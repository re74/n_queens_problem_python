#  The N Queens Problem
* The project is aimed to find a solution to the N-queens problem.
* The problem is to locate N queens on a board of size NxN so that each queen is not attacking any other queen on the board.
* The more detaled description of the problem can be found [here](https://en.wikipedia.org/wiki/Eight_queens_puzzle)
* The program presents only one solution in form of a NxN matrix where zeroes represent empty cells
and ones represent queens' positions.
* The program works for N in range [1;10].

## Usage

* Clone the repository.
* Inside the src folder run

```sh
python3 main.py
```

*When prompted, enter the size of the board in the range [1,10].
*Observe the output.

##  Output example
```sh
Enter a board size in range 1 to 10 (inclusive): 5
Solving iteratively... 
One solution is (queens are marked with 1): 
1 0 0 0 0 
0 0 1 0 0 
0 0 0 0 1 
0 1 0 0 0 
0 0 0 1 0 
```

## Maintainers

@MariaNema
