
#
# @desc Prints the board to the standard output. Board is represented as
#    a 2D array of size `board_size` x `board_size`. 
#    Zeroes represent empty cells. Ones represent queens.
# @param board_size The size of one side of the quadratic board.
# @param queens The array of placed queens where queens[i] holds the column position of the queen on the ith row.  
#

def print_board (board_size, queens):
    for r in range(board_size):
        for c in range (board_size):
            if queens[r] == c : 
                print( '1', end=' ')
            else:
                print( '0',end =' ')
        print()

#
# @desc Checks if the board is valid, i.e. the new queen wouldn't attack 
#    the queens that already exist on the board. 
# @param n_placed_queens The number of placed queens, including the new(last) one.
# @param queens The array of placed queens where queens[i] holds the column position of the queen on the ith row.  
# @returns true if the board with the new queen is valid, and false otherwise  
#

def is_valid_board (n_placed_queens, queens):
    for  r in range (n_placed_queens-1):
        #if there are any queens on the column or diagonals
        if queens[r] == queens[n_placed_queens-1] or abs(n_placed_queens-1-r)==abs(queens[n_placed_queens-1]-queens[r]):
            return False                 
    return True

#
# @desc Solves the n-queens problem iteratively. 
# @param board_size The size of one side of the quadratic board.
# @queens The array of placed queens where queens[i] holds the column position of the queen on the ith row.
# @returns true if the problem is solved, and false otherwise   
#
def solve_n_queens_pr_iteratively (board_size, queens):
    n_placed_queens = 0
    start_c = 0 #column position to start search.
    r = 0
    #for each row
    while r != board_size:
        #for possible column positions
        for c in range(start_c,board_size):
            #place queen
            queens[r] = c
            n_placed_queens += 1

            #if the board with the new queen is not valid
            if not is_valid_board(n_placed_queens, queens):
                #remove the queen
                queens[r]=-1
                n_placed_queens -= 1
            else: 
                break
    
        #if didn't find place for the new queen
        if n_placed_queens == r:
            #if the new queen was the queen on the first row
            if r == 0:
                return False
            #correct the column position to start the search 
            #when replacing previous queen
            start_c = 1 + queens[r-1]
            #remove previous queen from its place
            queens[r-1]=-1
            n_placed_queens-=1
            #to replace previous queen on new iteration
            r-=2            
        else:
            #to start from column 0 when placing the next queen
            start_c=0
        r += 1
    return True    