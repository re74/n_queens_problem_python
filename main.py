from n_queens_problem import print_board, solve_n_queens_pr_iteratively
# C++ program that receives an integer n in the range [1, 10], 
#and prints out at least one solution (if any exist) 
#to the problem of n-queen. 
#(In this problem, n queens should be put on an n*n chess board
# such that no queen can attack any other.)
# The solution represents the board. Ones represent queens,
# and zeroes represent empty cells.
# If no solution exist, the corresponding message is presented.*/
 
def main():   
    board_size = 0
    #Get the board size
    while not (board_size > 0 and board_size < 11):
        board_size = int(input( "Enter a board size in range 1 to 10 (inclusive): "))
          
    # Initiate the queens array   
    queens = [-1 for i in range (board_size)]    
    
    #Solve the problem iteratively
    print ("Solving iteratively... ")
    if solve_n_queens_pr_iteratively(board_size, queens):
        print ("One solution is (queens are marked with 1): ")
        print_board(board_size, queens)
    else:
        print("No solution found ")   

main()